# app.py
from flask import Flask, render_template, request, redirect, url_for
import psycopg2
import os

app = Flask(__name__)

# Replace these values with your PostgreSQL connection details

DB_NAME = os.environ.get('DB_NAME', 'todo_database')
DB_USER = os.environ.get('DB_USER', 'postgres')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'mysecretpassword')
DB_HOST = os.environ.get('DB_HOST', 'postgres')
DB_PORT = os.environ.get('DB_PORT', '5432')

# Function to connect to the PostgreSQL database

def connect_db():
    conn = psycopg2.connect(
        dbname=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT
    )
    return conn

# Create a cursor object to execute SQL queries
def create_cursor():
    conn = connect_db()
    cur = conn.cursor()
    return conn, cur

# Close database connection and cursor

def close_db(conn, cur):
    cur.close()
    conn.close()

# Initialize the database if it doesn't exist
def init_db():
    conn, cur = create_cursor()
    cur.execute('''
        CREATE TABLE IF NOT EXISTS todos (
            id SERIAL PRIMARY KEY,
            task TEXT NOT NULL,
            completed BOOLEAN NOT NULL
        )
    ''')
    conn.commit()
    close_db(conn, cur)

# Function to get all to-do tasks
def get_todos():
    conn, cur = create_cursor()
    cur.execute("SELECT * FROM todos")
    todos = cur.fetchall()
    close_db(conn, cur)
    return todos

# Function to create a new to-do task
def create_todo(task):
    conn, cur = create_cursor()
    cur.execute("INSERT INTO todos (task, completed) VALUES (%s, %s)", (task, False))
    conn.commit()
    close_db(conn, cur)

# Function to mark a to-do task as completed
def mark_completed(todo_id):
    conn, cur = create_cursor()
    cur.execute("UPDATE todos SET completed = TRUE WHERE id = %s", (todo_id,))
    conn.commit()
    close_db(conn, cur)

# Function to delete a to-do task
def delete_todo(todo_id):
    conn, cur = create_cursor()
    cur.execute("DELETE FROM todos WHERE id = %s", (todo_id,))
    conn.commit()
    close_db(conn, cur)

# Initialize the database on app startup
init_db()

# Routes
@app.route('/')
def index():
    todos = get_todos()
    return render_template('index.html', todos=todos)

@app.route('/add', methods=['POST'])
def add():
    task = request.form['task']
    create_todo(task)
    return redirect(url_for('index'))

@app.route('/complete/<int:todo_id>', methods=['GET'])
def complete(todo_id):
    mark_completed(todo_id)
    return redirect(url_for('index'))

@app.route('/delete/<int:todo_id>', methods=['GET'])
def delete(todo_id):
    delete_todo(todo_id)
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
